<?php 

//memanggil koneksi php
 require_once("koneksi.php");

 $sql_get = "SELECT * FROM roti";
 //kita berikan querynya
 $query_kb = mysqli_query($koneksi, $sql_get);
 $results = [];//result yang akan kita  buat baru
 
 //perulangan
 
 while($row = mysqli_fetch_assoc($query_kb)){
 $results[] = $row;//datanya disimpan dalam sebuah array results

 }


?>


<!DOCTYPE html>
<html>
<head>
	<title>Penjualan Roti Secara Online - Menampilkan Data dari Database</title>
	<<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
 <center>
 	<div class="judul">
 		<h1>Roti</h1>
 		<h2>Menampilkan Data dari Database</h2>
 	</div>
 	<br/>

 	<br/>
 	<table border="1" class="table">
    <tr>

     <th>id_Roti</th>
     <th>nama_Roti/th>
     <th>harga_Roti</th>
     <th>Opsi</th>
 </tr>

<?php 
    foreach($results as $result) :
 ?>
  <tr>

     <td><?php echo $result['id_Roti']; ?></td>
     <td><?php echo $result['nama_Roti']; ?></td>
     <td><?php echo $result['harga_Roti']; ?></td>
     <td>
        <a href="edit.php?id_Roti=<?=$result['id_Roti'];?>">Edit</a> ||
        <a href="hapus.php?id_Roti=<?=$result['id_Roti'];?>">Hapus</a>
 
     </td>
   </tr>

 <?php 
  endforeach;
 ?>

</table>
<br><br><a class="tombol" href="input.php">+ Tambah Data Baru</a>
</center>
</body>
</html>